# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from types import MethodType
from typing import Any, Callable, List  # noqa

from chalice.app import Request  # noqa


def urljoin(*args):
    return os.path.join("/", *[str(x).lstrip("/").rstrip("/") for x in args])


def url_for(self, view_name, **kwargs):
    stage = self.current_request.context["stage"]
    for path, methods in self.routes.items():
        for method in methods.keys():
            if self.routes[path][method].view_name == view_name:
                missing = []
                format_kwds = {}
                for arg in self.routes[path][method].view_args:
                    value = kwargs.pop(arg)
                    if value is None:
                        missing.append(arg)
                    else:
                        format_kwds[arg] = value
                if len(missing) > 0:
                    raise ValueError(
                        f'Missing values for {",".join(missing)} on route {path}'
                    )

                return urljoin(stage, path.format(**format_kwds))

    return None


def register_blueprint(self, blueprint, url_prefix=None):

    for blueprint_route in blueprint.routes:
        path, view_func, kwargs = blueprint_route

        if url_prefix is None:
            constructed_path = path
        else:
            constructed_path = urljoin(url_prefix, path.lstrip("/"))

        view_name = kwargs.pop("name", view_func.__name__)
        kwargs["name"] = ".".join([blueprint.name, view_name])

        self._add_route(constructed_path, view_func, **kwargs)

    blueprint.REGISTERED_APP = self


def blueprintify(app):
    app.register_blueprint = MethodType(register_blueprint, app)
    app.url_for = MethodType(url_for, app)


class Blueprint(object):

    REGISTERED_APP = None

    def __init__(self, name):  # type: (str) -> None
        self.name = name  # type: str
        self.routes = []  # type: List

    def url_for(self, view_name):  # type: (str) -> str
        if view_name.startswith("{}.".format(self.name)):
            pass
        elif view_name.startswith(".."):
            view_name = view_name.lstrip(".")
        else:
            view_name = "{}.{}".format(self.name, view_name.lstrip("."))

        if self.REGISTERED_APP is None:
            raise RuntimeError("Blueprint must be registered first!")

        stage = self.app.current_request.context["stage"]
        for path, methods in self.REGISTERED_APP.routes.items():
            for method in methods.keys():
                found_name = self.REGISTERED_APP.routes[path][method].view_name
                if found_name == view_name:
                    return urljoin(stage, path)

        raise LookupError("No url found for {}".format(view_name))

    @property
    def app(self):
        if self.REGISTERED_APP is None:
            raise RuntimeError("Blueprint must be registered first!")

        return self.REGISTERED_APP

    @property
    def current_request(self):  # type: () -> Request
        if self.REGISTERED_APP is None:
            raise RuntimeError("Blueprint must be registered first!")

        return self.REGISTERED_APP.current_request

    def route(self, path, **kwargs):  # type: (str, **Any) -> Callable

        def _register_view(view_func):  # type: (Callable) -> Callable
            self.routes.append((path, view_func, kwargs))
            return view_func

        return _register_view
