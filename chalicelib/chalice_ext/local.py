# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from functools import wraps

from chalicelib.chalice_ext import urljoin


class local_harness:

    def __init__(self, app, stage=None):
        if app is None:
            raise ValueError("app must be supplied as a keyword arg")

        self.app = app
        self.stage = stage if stage else ""

    def __call__(self, f):

        @wraps(f)
        def decorated(*args, **kwargs):
            if "stage" not in self.app.current_request.context:
                self.app.current_request.context["stage"] = self.stage
            if "path" not in self.app.current_request.context:
                resource_path = self.app.current_request.context["resourcePath"]
                uri_params = self.app.current_request.uri_params
                self.app.current_request.context["path"] = urljoin(
                    self.stage, resource_path.format(**uri_params)
                )
            return f(*args, **kwargs)

        return decorated
