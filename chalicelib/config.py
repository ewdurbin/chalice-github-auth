# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import base64

import boto3

from chalicelib.auth.config import render_config

kms = boto3.client("kms")


def decrypt_env_var(env_var_name):
    encrypted = os.environ.get(env_var_name)
    if encrypted is None:
        return None

    plaintext = kms.decrypt(CiphertextBlob=base64.b64decode(encrypted))["Plaintext"]
    return plaintext.decode("utf-8")


# Secret Key for itsdangerous signing and verificattion
SECRET_KEY = decrypt_env_var("SECRET_KEY")

# GitHub OAuth App Consumer Keys and Secrets
GITHUB_CONSUMER_KEY = decrypt_env_var("GITHUB_CONSUMER_KEY")
GITHUB_CONSUMER_SECRET = decrypt_env_var("GITHUB_CONSUMER_SECRET")

# DynamoDB Table prefix for models
DYNAMODB_TABLE_PREFIX = os.environ.get("DYNAMODB_TABLE_PREFIX", "dev")

# Authomatic Configuration and Secret
AUTHOMATIC_CONFIG = render_config(GITHUB_CONSUMER_KEY, GITHUB_CONSUMER_SECRET)
AUTHOMATIC_SECRET = decrypt_env_var("AUTHOMATIC_SECRET")
