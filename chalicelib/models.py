
import os
import time
import uuid

from dataclasses import field

from .dynamoclasses import dynamoclass

prefix = os.environ.get("DYNAMODB_TABLE_PREFIX", "local")


def _gen_uuid():
    return str(uuid.uuid4())


def _gen_current_timestamp():
    return int(time.time())


@dynamoclass(table_name=f"{prefix}_user", partition_key_name="uuid")
class User:
    email_address: str
    username: str
    uuid: str = field(default_factory=_gen_uuid)
    created_at: int = field(default_factory=_gen_current_timestamp)
    updated_at: int = field(default_factory=_gen_current_timestamp)
    deleted_at: int = -1
    deleted: bool = False


@dynamoclass(
    table_name=f"{prefix}_user_oauth",
    partition_key_name="client_user_id",
    sort_key_name="client_id",
)
class UserOAuth:
    client_id: str
    client_user_id: str
    client_username: str
    user_uuid: str
    client_type: str
    encrypted_kms_data_key: str
    encrypted_access_token: str
    encrypted_refresh_token: str

    @property
    def token(self):
        return self.encrypted_access_token


@dynamoclass(
    table_name=f"{prefix}_bounties",
    partition_key_name="bounty_id",
)
class Bounty:
    created_user_uuid: str
    kind: str
    quantity: int
    bounty_id: str = field(default_factory=_gen_uuid)
    repository_id: str = None
    organization_id: str = None
    created_at: int = field(default_factory=_gen_current_timestamp)
    not_before: int = -1
    not_after : int = -1
    deleted_at: int = -1
    deleted: bool = False
