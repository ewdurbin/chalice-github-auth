# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from http.cookies import SimpleCookie
from types import MethodType

from chalice.app import Response
from itsdangerous import BadTimeSignature, SignatureExpired

from chalicelib.auth.adapter import ChaliceAdapter
from chalicelib.chalice_ext import Blueprint
from chalicelib.chalice_ext.local import local_harness
from chalicelib.models import User, UserOAuth

auth = Blueprint("auth")


def authenticated(self, required=True):
    authed = False

    cookies = SimpleCookie(self.current_request.headers.get("cookie", ""))
    user_uuid = cookies.get("user_uuid")
    if user_uuid:
        try:
            unsigned_user_uuid = self.signer.unsign(user_uuid.value, max_age=6 * 3600)
        except SignatureExpired:
            message = "Session+Expired%21+Log+in+again%21"
            location = self.url_for("index") + f"?message={message}"
            return Response(body="", status_code=302, headers={"Location": location})

        except BadTimeSignature:
            message = "Bad+Session%21+Log+in+again%21"
            location = self.url_for("index") + f"?message={message}"
            return Response(body="", status_code=302, headers={"Location": location})

        user_uuid = unsigned_user_uuid.decode("utf-8")
        self.current_request.context["user"] = User.get(
            partition_key=user_uuid, sort_key=None
        )
        authed = True

    if authed:
        return True, None

    if required:
        return False, Response(body="Unauthorized", status_code=403)

    return False, None


def authify(app):
    app.authenticated = MethodType(authenticated, app)


@auth.route("/login")
@local_harness(auth)
def login():
    auth.app.authenticated(required=False)
    user = auth.current_request.context.get("user", "")
    next_url = None
    if user:
        if auth.current_request.query_params:
            if "next" in auth.current_request.query_params:
                next_url = auth.url_for(
                    ".." + auth.current_request.query_params["next"]
                )
                if next_url is None:
                    next_url = auth.url_for("..index")
    return Response(
        body=auth.app.jinja.get_template("login.html").render(
            user=user, next_url=next_url
        ),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@auth.route("/logout")
@local_harness(auth)
def logout():
    domain = auth.current_request.headers["host"].split(":")[0]
    cookie = (
        f"user_uuid=deleted; Domain={domain}; Path=/; "
        "Max-Age=0; HttpOnly; SameSite=Strict"
    )
    if not auth.app.debug:
        cookie += "; Secure"
    return Response(
        body="Found",
        status_code=302,
        headers={"Set-Cookie": cookie, "Location": auth.url_for("..index")},
    )


@auth.route("/login/{providername}", methods=["GET", "POST"])
@local_harness(auth)
def login_provider(providername):
    if providername not in auth.app.config.AUTHOMATIC_CONFIG:
        return Response("Not Found", status_code=404)

    request = auth.current_request
    response = Response(body="", status_code=200, headers={})
    result = auth.app.authomatic.login(ChaliceAdapter(request, response), providername)
    if result:
        if result.user:
            result.user.update()
        user_oauth = UserOAuth.get(
            partition_key=result.user.id, sort_key=result.provider.consumer_key
        )
        if user_oauth is None:
            user = User(email_address=result.user.email, username=result.user.username)
            user.save()
            user_oauth = UserOAuth(
                client_id=result.provider.consumer_key,
                client_user_id=result.user.id,
                client_username=result.user.username,
                user_uuid=user.uuid,
                client_type=result.provider.name,
                encrypted_kms_data_key="null",
                encrypted_access_token=result.user.credentials.token,
                encrypted_refresh_token=result.user.credentials.refresh_token,
            )
            user_oauth.save()
        else:
            user_oauth.client_username = result.user.username
            user_oauth.encrypted_kms_data_key = "null"
            user_oauth.encrypted_access_token = result.user.credentials.token
            user_oauth.encrypted_refresh_token = result.user.credentials.refresh_token
            user_oauth.save()
            user = User.get(partition_key=user_oauth.user_uuid, sort_key=None)
        session = auth.app.signer.sign(f"{user.uuid}")
        domain = auth.current_request.headers["host"].split(":")[0]
        max_age = 6 * 3600
        cookie = (
            f'user_uuid={session.decode("utf-8")}; '
            f"Domain={domain}; Path=/; "
            f"Max-Age={max_age}; "
            f"HttpOnly; SameSite=Strict"
        )
        next_url = auth.url_for("..index")
        if not auth.app.debug:
            cookie += "; Secure"
        if auth.current_request.query_params:
            if "next" in auth.current_request.query_params:
                next_url = auth.url_for(
                    ".." + auth.current_request.query_params["next"]
                )
        return Response(
            body=auth.app.jinja.get_template("login.html").render(
                user=user, next_url=next_url
            ),
            status_code=200,
            headers={"Content-Type": "text/html", "Set-Cookie": cookie},
        )

    return Response(
        body=response.body, status_code=response.status_code, headers=response.headers
    )
