# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from authomatic.adapters import BaseAdapter

from http.cookies import SimpleCookie


class ChaliceAdapter(BaseAdapter):
    """
    Adatper for the |chalice|_ framework.
    """

    def __init__(self, request, response):
        """
        :param request:
            Instance of the :class:`chalice.app.Request` class.
        :param response:
            Instance of the :class:`chalice.app.Response` class.
        """
        self.request = request
        self.response = response

    @property
    def params(self):
        return self.request.query_params if self.request.query_params else {}

    @property
    def url(self):
        host = self.request.headers["host"]
        proto = self.request.headers.get("x-forwarded-proto", "http")
        path = self.request.context["path"]
        return f"{proto}://{host}{path}"

    @property
    def cookies(self):
        cookie = SimpleCookie(self.request.headers.get("cookie", ""))
        return {k: v.value for k, v in cookie.items()}

    def write(self, value):
        self.response.body += value

    def set_header(self, key, value):
        self.response.headers[key] = value

    def set_status(self, status):
        status_code, reason = status.split(" ", 1)
        self.response.status_code = int(status_code)
