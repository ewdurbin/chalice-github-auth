# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from authomatic.providers import oauth2


def render_config(GITHUB_CONSUMER_KEY, GITHUB_CONSUMER_SECRET):
    return {
        "github": {
            "class_": oauth2.GitHub,
            "consumer_key": GITHUB_CONSUMER_KEY,
            "consumer_secret": GITHUB_CONSUMER_SECRET,
            "access_headers": {"User-Agent": "Sticker-Bounty/1.0.0"},
            "scope": ["read:user", "read:org"],
        }
    }
