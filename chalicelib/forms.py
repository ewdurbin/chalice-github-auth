
import wtforms

from wtforms.fields.html5 import DateField


class ProjectBountyForm(wtforms.form.Form):
    project_name = wtforms.fields.StringField(
        u"Repository Full Name (sticker-bounty/sticker-bounty-app)",
        validators=[wtforms.validators.input_required()]
    )
    bounty_type = wtforms.fields.SelectField(
        u"What kind of contributions are eligible?",
        choices=[("any", "Any Contribution")],
        validators=[wtforms.validators.input_required()],
    )
    bounty_count = wtforms.fields.IntegerField(u"How many stickers?", validators=[wtforms.validators.input_required()])
    bounty_start = DateField(u"Contributions after this date are eligible")
    bounty_end = DateField(u"Contributions after this date are ineligible")


class OrganizationBountyForm(wtforms.form.Form):
    organization_name = wtforms.fields.StringField(
        u"Organization Name (sticker-bounty)",
        validators=[wtforms.validators.input_required()],
    )
    bounty_type = wtforms.fields.SelectField(
        u"What kind of contributions are eligible?",
        choices=[("any", "Any Contribution")],
        validators=[wtforms.validators.input_required()],
    )
    bounty_count = wtforms.fields.IntegerField(u"How many stickers?", validators=[wtforms.validators.input_required()])
    bounty_start = DateField(u"Contributions after this date are eligible")
    bounty_end = DateField(u"Contributions after this date are ineligible")
