# Chalice Login with GitHub

This is just an experiment in pushing [AWS Lambda](https://aws.amazon.com/lambda/) and [AWS API Gateway](https://aws.amazon.com/api-gateway/) too the edge with proclaimed "Microframework" [awslabs/chalice](https://github.com/aws/chalice/).

## Summary

This little application allows for "Sign In with GitHub" functionality in a Python based AWS Lambda application exposed via the AWS API Gateway.

It does this with no database, cookie based sessions, a little bit of Jinja and some weirdware written by [@EWDurbin](https://twitter.com/EWDurbin).

The weirdware is all in the `chalicelib` directory and includes a port of my work to [add modular applications to `chalice`](https://github.com/aws/chalice/pull/651), a `ChaliceAdapter` for [`authomatic`](https://authomatic.github.io/authomatic/), some hacks to make `url_for` work in all of that goofery, and some hax to make `chalice local` dev work.

## Live Demo!

Check it out right [here](https://82ir4pqxx7.execute-api.us-east-2.amazonaws.com/dev/).

## Using this

### Pre-Requisites

- Python 3.6 available on your local system
- `pipenv` available on your `PATH` ([Installation Instructions](http://pipenv.readthedocs.io/en/latest/install/#installing-pipenv))
- A GitHub Personal Access Token ([Docs](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/))
- A Client ID and Client Secret for a GitHub OAuth Application ([Docs](https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/))
- An Active AWS Account
  - Configuration must be set in one of:
    - Environment Variables ([Docs](https://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html))
    - AWS CLI configuration ([Docs](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html))

### Local Setup

1. Clone this repo!
  - `git clone https://gitlab.com/ewdurbin/chalice-github-auth.git`
2. Get in there!
  - `cd chalice-github-auth`
3. Setup your `pipenv`
  - `pipenv install`
4. "Enter" your new environment:
  - `pipenv shell`
5. Setup Configuration (Enter stuff as prompted):
  - `./setup`
  - <pre>
    KMS Key ID, or "create" to create one: create
    GitHub Access Token: 
    GitHub OAuth Application Consumer Key: 
    GitHub OAuth Application Consumer Secret: 
    creating KMS Key!
    created KMS Key with ID: deadbeef-dead-beef-dead-beefdeadbeef
    writing local env file...
    writing chalice configurations...
    Success!
      Now reload your pipenv! "exit", then "pipenv shell"
      Then start up your local copy! "chalice local"
      Then check out the demo! "open http://localhost:8000/demo"
    </pre>

6. Do what the dang script said!

### Deploy

From within your `pipenv`: `chalice deploy` :sparkles:

## Built with:
- `pipenv`
  - Easing the pain of Python environment management
- `chalice`
  - Local dev and deployment to AWS Lambda and API Gateway
- `Jinja2`
  - Templates, cause JSON APIs are ugly in browsers
- `boto3`
  - To talk to KMS
- `itsdangerous`
  - Verifiable and Expirable session tokens
- `click`
  - Command Line Setup

## Yo, this is really fucking cool!

Thank You!

I’m currently available for short term contract based work.

I’d love to help your team build neat shit with Python! I'm heavily biased towards Open Source, but am happy to work on the right kinds of proprietary systems.

Email: hire@ernest.ly
