# Copyright 2018 Ernest W. Durbin III
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import collections

from functools import wraps
from urllib.parse import parse_qs

import github3

from authomatic import Authomatic
from chalice import Chalice, Response
from itsdangerous import TimestampSigner
from jinja2 import Environment, PackageLoader
from werkzeug.datastructures import ImmutableMultiDict

from chalicelib.auth.blueprint import (auth, authify)
from chalicelib.chalice_ext import blueprintify, urljoin
from chalicelib.chalice_ext.local import local_harness
from chalicelib.config import (
    AUTHOMATIC_CONFIG,
    AUTHOMATIC_SECRET,
    DYNAMODB_TABLE_PREFIX,
    GITHUB_CONSUMER_KEY,
    GITHUB_CONSUMER_SECRET,
    SECRET_KEY,
)
from chalicelib.forms import ProjectBountyForm, OrganizationBountyForm
from chalicelib.models import Bounty, UserOAuth

app = Chalice(app_name="stickerbounty")
app.debug = True if os.environ.get("CHALICE_LOCAL") else False
blueprintify(app)
authify(app)
app.config = type("Config", (object,), {})()

app.register_blueprint(auth, url_prefix="/auth")

app.jinja = Environment(loader=PackageLoader("chalicelib", "templates"))
app.jinja.globals.update(app=app)
app.jinja.globals.update(url_for=app.url_for)

app.signer = TimestampSigner(SECRET_KEY)

app.authomatic = Authomatic(
    AUTHOMATIC_CONFIG, AUTHOMATIC_SECRET, secure_cookie=(not app.debug)
)
setattr(app.config, "AUTHOMATIC_CONFIG", AUTHOMATIC_CONFIG)


@app.route("/")
@local_harness(app)
def index():
    app.authenticated(required=False)
    user = app.current_request.context.get("user", None)
    return Response(
        body=app.jinja.get_template("index.html").render(user=user),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@app.route("/give")
@local_harness(app)
def give():
    app.authenticated(required=True)
    user = app.current_request.context.get("user", None)
    return Response(
        body=app.jinja.get_template("give.html").render(user=user),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@app.route("/give/projects")
@local_harness(app)
def give_projects():
    app.authenticated(required=True)
    user = app.current_request.context.get("user", None)
    user_oauth = UserOAuth.query(
        index_name="user_uuid-index",
        partition_key_name="user_uuid",
        partition_key=user.uuid,
    )
    github_api = github3.GitHub(
        username=user_oauth.client_username, token=user_oauth.token
    )
    repositories = [r for r in github_api.repositories(type="public") if not r.fork]
    return Response(
        body=app.jinja.get_template("give_projects.html").render(
            user=user, projects=repositories, client_id=user_oauth.client_id
        ),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@app.route(
    "/give/project/{organization}/{project_name}",
    methods=["GET", "POST"],
    content_types=["application/x-www-form-urlencoded", "application/json"],
)
@local_harness(app)
def give_project(organization, project_name):
    app.authenticated(required=True)
    user = app.current_request.context.get("user", None)
    user_oauth = UserOAuth.query(
        index_name="user_uuid-index",
        partition_key_name="user_uuid",
        partition_key=user.uuid,
    )
    github_api = github3.GitHub(
        username=user_oauth.client_username, token=user_oauth.token
    )
    collaborator = False
    try:
        repository = github_api.repository(organization, project_name)
    except github3.exceptions.NotFoundError:
        return Response(
            body=app.jinja.get_template("message.html").render(
                message=f"NotFound: No such repository {organization}/{project_name} found!",
                user=user,
            ),
            status_code=404,
            headers={"Content-Type": "text/html"},
        )

    user_repos = [r.full_name for r in github_api.repositories()]
    collaborator = repository.full_name in user_repos
    if not collaborator:
        return Response(
            body=app.jinja.get_template("message.html").render(
                message=f"Unauthorized: You must be a collaborator on {organization}/{project_name} to post a bounty!",
                user=user,
            ),
            status_code=401,
            headers={"Content-Type": "text/html"},
        )

    bounty = Bounty.query(
        index_name="repository_id-index",
        partition_key_name="repository_id",
        partition_key=repository.full_name,
    )
    project_form = ProjectBountyForm(
        ImmutableMultiDict(parse_qs(app.current_request.raw_body.decode()))
    )
    project_form.project_name.data = f"{organization}/{project_name}"
    if bounty:
        project_form.bounty_count.data = bounty.quantity
        project_form.bounty_type.data = bounty.kind
    if app.current_request.method == "POST" and project_form.validate():
        if bounty is None:
            bounty = Bounty(
                created_user_uuid=user.uuid,
                kind=project_form.bounty_type.data,
                quantity=project_form.bounty_count.data,
                repository_id=repository.full_name,
            )
        else:
            bounty.kind = project_form.bounty_type.data
            bounty.quantity = project_form.bounty_count.data
            bounty.repository_id = repository.full_name
        bounty.save()
    return Response(
        body=app.jinja.get_template("give_project.html").render(
            user=user,
            organization=organization,
            project_name=project_name,
            project_form=project_form,
        ),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@app.route("/give/organizations")
@local_harness(app)
def give_organizations():
    app.authenticated(required=True)
    user = app.current_request.context.get("user", None)
    user_oauth = UserOAuth.query(
        index_name="user_uuid-index",
        partition_key_name="user_uuid",
        partition_key=user.uuid,
    )
    github_api = github3.GitHub(
        username=user_oauth.client_username, token=user_oauth.token
    )
    organizations = [
        github_api.organization(o.login) for o in github_api.organizations()
    ]
    return Response(
        body=app.jinja.get_template("give_organizations.html").render(
            user=user, organizations=organizations, client_id=user_oauth.client_id
        ),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@app.route(
    "/give/organization/{organization}",
    methods=["GET", "POST"],
    content_types=["application/x-www-form-urlencoded", "application/json"],
)
@local_harness(app)
def give_organization(organization):
    app.authenticated(required=True)
    user = app.current_request.context.get("user", None)
    user_oauth = UserOAuth.query(
        index_name="user_uuid-index",
        partition_key_name="user_uuid",
        partition_key=user.uuid,
    )
    github_api = github3.GitHub(
        username=user_oauth.client_username, token=user_oauth.token
    )
    owner = False
    try:
        organization_obj = github_api.organization(organization)
    except github3.exceptions.NotFoundError:
        return Response(
            body=app.jinja.get_template("message.html").render(
                message=f"NotFound: No such organization {organization} found!",
                user=user,
            ),
            status_code=404,
            headers={"Content-Type": "text/html"},
        )

    bounty = Bounty.query(
        index_name="organization_id-index",
        partition_key_name="organization_id",
        partition_key=organization_obj.login,
    )
    organization_form = OrganizationBountyForm(
        ImmutableMultiDict(parse_qs(app.current_request.raw_body.decode()))
    )
    organization_form.organization_name.data = organization
    if bounty:
        organization_form.bounty_count.data = bounty.quantity
        organization_form.bounty_type.data = bounty.kind
    if app.current_request.method == "POST" and organization_form.validate():
        if bounty is None:
            bounty = Bounty(
                created_user_uuid=user.uuid,
                kind=organization_form.bounty_type.data,
                quantity=organization_form.bounty_count.data,
                organization_id=organization_obj.login,
            )
        else:
            bounty.kind = organization_form.bounty_type.data
            bounty.quantity = organization_form.bounty_count.data
            bounty.organization_id = organization_obj.login
        bounty.save()
    return Response(
        body=app.jinja.get_template("give_organization.html").render(
            user=user,
            organization=organization,
            organization_name=organization_obj.login,
            organization_form=organization_form,
        ),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


@app.route("/claim")
@local_harness(app)
def claim():
    app.authenticated(required=True)
    user = app.current_request.context.get("user", None)
    user_oauth = UserOAuth.query(
        index_name="user_uuid-index",
        partition_key_name="user_uuid",
        partition_key=user.uuid,
    )
    github_api = github3.GitHub(
        username=user_oauth.client_username, token=user_oauth.token
    )
    organizations = [
        github_api.organization(o.login) for o in github_api.organizations()
    ]
    bounties = Bounty.all()
    return Response(
        body=app.jinja.get_template("claim.html").render(user=user, bounties=bounties),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )


def check_did_user_contribute_to_repo(github_api, owner=None, repo=None, user=None):
    repo = github_api.repository(owner, repo)
    contributors = set([c.login for c in repo.contributors()])
    return (user in contributors)


def check_did_user_contribute_to_org(github_api, org=None, user=None):
    org_contributors = collections.defaultdict(list)
    organization = github_api.organization(org)
    repos = organization.repositories(type="public")
    for repo in repos:
        contributors = repo.contributors()
        for contributor in contributors:
            org_contributors[contributor.login].append(repo.name)
    return (user in org_contributors), org_contributors[user]


@app.route("/claim/{bounty_id}")
@local_harness(app)
def claim_bounty(bounty_id):
    app.authenticated(required=True)
    bounty = Bounty.get(partition_key=bounty_id, sort_key=None)
    user = app.current_request.context.get("user", None)
    if bounty is None:
        return Response(
            body=app.jinja.get_template("message.html").render(
                message=f"NotFound: No bounty with that id found!", user=user
            ),
            status_code=404,
            headers={"Content-Type": "text/html"},
        )

    user_oauth = UserOAuth.query(
        index_name="user_uuid-index",
        partition_key_name="user_uuid",
        partition_key=user.uuid,
    )
    github_api = github3.GitHub(
        username=user_oauth.client_username, token=user_oauth.token
    )
    claim_valid = False
    if bounty.organization_id:
        claim_valid, repos = check_did_user_contribute_to_org(
            github_api, org=bounty.organization_id, user=user_oauth.client_username
        )
    elif bounty.repository_id:
        owner, repo = bounty.repository_id.split("/")
        claim_valid = check_did_user_contribute_to_repo(
            github_api, owner=owner, repo=repo, user=user_oauth.client_username
        )
    return Response(
        body=app.jinja.get_template("claim_bounty.html").render(
            user=user, bounty=bounty, claim_valid=claim_valid
        ),
        status_code=200,
        headers={"Content-Type": "text/html"},
    )
